<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="styles.css">
    <link href="bootstrap.min.css" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Flex_CSS</title>
</head>
<header>
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
          <div class="container ">
            <div class="navbar-header ">
              <a class="nav-brand navbar-text" type="button" href="#home"><img src="flexvsgrid-featured1.png"></a>
            </div>
            <div class="nav text-light navbar-right">
              <ul class="nav navbar-nav">
                <li><a href="#historique">Découvrir Flex et CSS Grid</a></li>
                <li><a href="#utiliser">Comment les utiliser?</a></li>
                <li><a href="#navigateurs">Avec quels navigateurs?</a></li>
                <li><a href="#comparaison">Différences et complémentarités</a></li>
              </ul>
            </div>
          </div>
        </nav>

        <div class ="title"><h1>Flex Vs Grid<h1></div>
        
    </header>
<body>

        <div class="container">
            <div class="flex"> <!--display : flex  flex-direction: column -->
                <div id="historique">
                  <p>Le module Flexible Box Layout est proposé en 2009 sous forme de "First Public Draft"2 par le groupe de travail CSS du W3C. Après quatre publications successives, Flex Box passe en 2012 au stade de "Candidate Recommendation".<br>
                  Flexbox est une méthode de mise en page selon un axe principal, permettant de disposer des éléments en ligne ou en colonne. Les éléments se dilatent ou se rétractent pour occuper l’espace disponible.La spécification Flexbox apporte une panoplie de fonctionnalités et d’outils qu’il est possible de combiner.<br>
                
                  </p>
                </div>
                <div id="utiliser">
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer mauris nisi, egestas faucibus rutrum at, blandit vel nunc. Nunc ipsum eros, eleifend at vehicula ac, fringilla in leo. Nunc condimentum egestas massa ac sagittis. Etiam facilisis lectus sit amet ligula consequat, nec aliquet enim porttitor. Vestibulum felis ex, pretium sed pretium sed, porta vel augue. Quisque augue neque, scelerisque sed erat non, volutpat facilisis risus. </p>


                </div>
                <div id="navigateurs">
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer mauris nisi, egestas faucibus rutrum at, blandit vel nunc. Nunc ipsum eros, eleifend at vehicula ac, fringilla in leo. Nunc condimentum egestas massa ac sagittis. Etiam facilisis lectus sit amet ligula consequat, nec aliquet enim porttitor. Vestibulum felis ex, pretium sed pretium sed, porta vel augue. Quisque augue neque, scelerisque sed erat non, volutpat facilisis risus. </p>
                </div>
                <div id="comparaison">
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer mauris nisi, egestas faucibus rutrum at, blandit vel nunc. Nunc ipsum eros, eleifend at vehicula ac, fringilla in leo. Nunc condimentum egestas massa ac sagittis. Etiam facilisis lectus sit amet ligula consequat, nec aliquet enim porttitor. Vestibulum felis ex, pretium sed pretium sed, porta vel augue. Quisque augue neque, scelerisque sed erat non, volutpat facilisis risus. </p>
                </div>
                
            </div>
            <div class="grid"> <!--display : grid-->
                <div class="historique2">
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer mauris nisi, egestas faucibus rutrum at, blandit vel nunc. Nunc ipsum eros, eleifend at vehicula ac, fringilla in leo. Nunc condimentum egestas massa ac sagittis. Etiam facilisis lectus sit amet ligula consequat, nec aliquet enim porttitor. Vestibulum felis ex, pretium sed pretium sed, porta vel augue. Quisque augue neque, scelerisque sed erat non, volutpat facilisis risus. </p>
                </div>
                <div class="utiliser2">
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer mauris nisi, egestas faucibus rutrum at, blandit vel nunc. Nunc ipsum eros, eleifend at vehicula ac, fringilla in leo. Nunc condimentum egestas massa ac sagittis. Etiam facilisis lectus sit amet ligula consequat, nec aliquet enim porttitor. Vestibulum felis ex, pretium sed pretium sed, porta vel augue. Quisque augue neque, scelerisque sed erat non, volutpat facilisis risus. </p>
                </div>
                <div class="navigateurs2">
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer mauris nisi, egestas faucibus rutrum at, blandit vel nunc. Nunc ipsum eros, eleifend at vehicula ac, fringilla in leo. Nunc condimentum egestas massa ac sagittis. Etiam facilisis lectus sit amet ligula consequat, nec aliquet enim porttitor. Vestibulum felis ex, pretium sed pretium sed, porta vel augue. Quisque augue neque, scelerisque sed erat non, volutpat facilisis risus. </p>
                </div>
                <div class="comparaison2">
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer mauris nisi, egestas faucibus rutrum at, blandit vel nunc. Nunc ipsum eros, eleifend at vehicula ac, fringilla in leo. Nunc condimentum egestas massa ac sagittis. Etiam facilisis lectus sit amet ligula consequat, nec aliquet enim porttitor. Vestibulum felis ex, pretium sed pretium sed, porta vel augue. Quisque augue neque, scelerisque sed erat non, volutpat facilisis risus. </p>  
                </div>


            </div>
       

        </div>
        </body>
<footer>

</footer>   

</html>